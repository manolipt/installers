#!/bin/zsh

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt

ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

hwclock --systohc

sed -i '/en_US.UTF-8/s/^#//g' /etc/locale.gen
sed -i '/ja_JP.UTF-8/s/^#//g' /etc/locale.gen
locale-gen

echo KEYMAP=us > /etc/vconsole.conf

echo "Next steps:\n"
echo "1. Set hostname in \'/etc/hostname\'."
echo "2. If needed, edit mkinitcpio.conf and run \'mkinitcpio -P\'."
echo "3. Set the root password using passwd"
echo "4. Install and setup the desired bootloader"
echo "5. umount -R /mnt"
echo "\nThen reboot and you're good to go!"

