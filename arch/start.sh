#!/bin/zsh

loadkeys us
setfont ter-132b
echo "Boot Mode: $(cat /sys/firmware/efi/fw_platform_size)-bit"

echo "Next Steps:\n"
echo "1. Connect to the internet using \'ip link\' for ethernet and \'iwctl\' for wifi."
echo "2. Update system clock using timedatectl"
echo "3. Encrypt and partition disks as needed"
echo "4. Mount the filesystems"
echo "5. Use pacstram to install essential packages"
echo "\nAfter that, run the next script"
